package spot_order

type SpotOrderStatus = string

const (
	SpotOrderStatusNew      SpotOrderStatus = "new"
	SpotOrderStatusPending  SpotOrderStatus = "pending"
	SpotOrderStatusFilled   SpotOrderStatus = "filled"
	SpotOrderStatusCanceled SpotOrderStatus = "canceled"
)

type SpotOrderType = string

const (
	SpotOrderTypeMarket SpotOrderType = "market"
	SpotOrderTypeLimit  SpotOrderType = "limit"
)
