package price

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetPriceDowner(t *testing.T) {
	price, err := GetPriceDowner("0.25563604", "0.0001")
	assert.NoError(t, err)
	assert.Equal(t, "0.2556", price)
}

func TestGetPriceUpper(t *testing.T) {
	price, err := GetPriceUpper("0.25563604", "0.0001")
	assert.NoError(t, err)
	assert.Equal(t, "0.2557", price)
}
