package price

import (
	"github.com/shopspring/decimal"
)

func GetPriceUpper(expectPrice string, priceStep string) (string, error) {
	price := decimal.Zero
	exp, err := decimal.NewFromString(expectPrice)
	if err != nil {
		return "", err
	}
	step, err := decimal.NewFromString(priceStep)
	if err != nil {
		return "", err
	}
	sCount := exp.Div(step).Sub(decimal.NewFromFloat(2.0)).Floor()
	price = step.Mul(sCount)
	for {
		if price.GreaterThanOrEqual(exp) {
			break
		}
		price = price.Add(step)
	}
	return price.String(), nil
}

func GetPriceDowner(expectPrice string, priceStep string) (string, error) {
	price := decimal.NewFromFloat(0.0)
	exp, err := decimal.NewFromString(expectPrice)
	if err != nil {
		return "", err
	}
	step, err := decimal.NewFromString(priceStep)
	if err != nil {
		return "", err
	}
	sCount := exp.Div(step).Sub(decimal.NewFromFloat(2.0)).Floor()
	price = step.Mul(sCount)
	for {
		if price.Add(step).GreaterThanOrEqual(exp) {
			break
		}
		price = price.Add(step)
	}
	return price.String(), nil
}
