package binance_order

type OrderStatus = int

const (
	OrderStatusCreated  OrderStatus = 1
	OrderStatusCanceled OrderStatus = 2
	OrderStatusFilled   OrderStatus = 3
)
