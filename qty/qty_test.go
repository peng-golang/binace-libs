package qty

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetQtyByPrice(t *testing.T) {
	rs, err := GetQtyByPrice("2.861", "0.1", "0.1", "10")
	assert.NoError(t, err)
	assert.NotEmpty(t, rs)
}
