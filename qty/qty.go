package qty

import (
	"errors"
	"github.com/shopspring/decimal"
)

func GetQtyByPrice(price string, minQty string, step string, usdtQty string) (string, error) {
	qtyD, err := decimal.NewFromString(minQty)
	if err != nil {
		return "", err
	}
	priceD, err := decimal.NewFromString(price)
	if err != nil {
		return "", err
	}
	usdtD, err := decimal.NewFromString(usdtQty)
	if err != nil {
		return "", err
	}
	stepD, err := decimal.NewFromString(step)
	if err != nil {
		return "", err
	}
	if stepD.Equal(decimal.Zero) {
		return "", errors.New("step cannot be 0")
	}
	sCount := usdtD.Div(stepD.Mul(priceD)).Sub(decimal.NewFromFloat(2.0)).Floor()
	qtyD = stepD.Mul(sCount)
	for {
		if qtyD.Mul(priceD).GreaterThanOrEqual(usdtD) {
			break
		}
		qtyD = qtyD.Add(stepD)
	}
	return qtyD.String(), nil
}
